## Ejercicio - Promedio de calificaciones

Un promedio es la media aritmética y se calcula sumando un grupo de números y dividiendo a continuación por el recuento de dichos números.


Crea el método `average` que recibe como parámetro un `Array` de números y regresa el promedio.

Recuerda que el resultado de las comparaciones debe ser true.

>Restricción: No usar las estructuras iterativas `for`, `while`, `do..while`, `until`, `each`.

```ruby
#Driver code

p average([8, 8, 7, 6, 9]) == 7.6
p average([5, 5, 5, 8, 8, 7, 7, 7]) == 6.5
p average([5, 5, 5, 8, 8, 7, 7, 2]) == 5.875
```

